


# VBBBQ

VBBBQ - aka Very Big Barbecue - is a website created to fill a gap that its designer felt : there is no clear place on the web dedicated to relaxed discussions between strangers and involving a geographic dimension to permit common projects and to facilitate the birth of possible friendships.

The procedure is given in command lines, working under Ubuntu 18.04 but steps can be reproduced with other operating systems, including Windows and OS X in principle (not tested).

> The VBBBQ code is divided in two Git repositories : one dedicated to the frontend (Vue.js) and one dedicated to the backend (SailsJS).  The current one is a superproject which imports both as submodules for conveniance.

## Installation

First, clone this repository. 
```
git clone https://gitlab.com/Cart3sianBear/vbbbq
```
Then, you have to fetch submodules content :
```
cd vbbbq
git submodule init
git submodule update
```
The versions chosen for the provided submodules are consistent but not necessarily the last commits. If you want them, you have to do `git submodule update --remote`. During development, the `--merge` flag can also be useful.

Whenever you do a `git pull` of this superproject, you should also do a `git submodule update`. Please refer to Git Submodules documentation for more information.

### Node.js & npm
```
sudo apt-get install -y npm
sudo npm install -g npm
sudo npm install -g n
sudo n 11
sudo chown -R $USER:$GROUP ~/.npm
sudo chown -R $USER:$GROUP ~/.config'
```

### MySQL
Because of specific code written for the recommendation system, MySQL is the only database currently supported to store all users informations and discussions data.
```
wget http://repo.mysql.com/mysql-apt-config_0.8.10-1_all.deb
sudo dpkg -i mysql-apt-config_0.8.10-1_all.deb
rm mysql-apt-config_0.8.10-1_all.deb
sudo apt update
sudo apt install -y mysql-server
```

### Redis
Redis is used to store session tokens.
```
sudo apt install -y redis-server
sudo systemctl start redis
```

### Nominatim
The Nominatim geocatcher is used to give access to geographic data. A local installation is recommended - please refer to their documentation - but Nominatim generously provides a public demo API ; which should be used with wisdom.

### Mailgun
Mailgun is used to garantee the authentication of users with a valid email address, if thus configured. You would need an account on their website.

### Node modules
In both `./vbbbq-frontend` and `./vbbbq-backend`, execute :
```
npm install
```
## Configuration

### MySQL
You should change the MySQL password (`password` by default) :
```
sudo mysql -h localhost -u root -p <<SCRIPT
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
SCRIPT
echo -e '\n !!! Next type \"password\", then answer no, then several times yes !!!'
sudo mysql_secure_installation
sudo systemctl restart mysql
systemctl status mysql.service
```
Then, you can create a database for VBBBQ :
```
mysql -h localhost -u root -p <<SCRIPT
CREATE DATABASE vbbbq;
SHOW DATABASES;
SCRIPT
```

### Redis
You have to define a password for Redis. First generate a strong one :
```
echo "seed-to-change" | sha256sum
```
Then, edit the configuration file :
```
sudo nano /etc/redis/redis.conf
```
Search (`Ctrl+W`) for the word `requirepass`, uncomment the corresponding line and replace `foobared` with the generated password. Then, restart Redis :
```
sudo systemctl restart redis
systemctl status redis
```
### Environments
VBBBQ  can be run in two modes : `development` and `production`. Depending of the mode(s) you plan to use, you have to edit `./development.env` and/or `./production.env` textfiles according to your needs.

The two last parameters `RECOMMENDED_CONVERSATIONS_ALPHA` and `RECOMMENDED_CONVERSATIONS_TAU` are related to VBBBQ's recommendation algorithm. The first one (between `0` and `1`) gives a weight to randomness in this algorithm, and the second one (`>0`) is the characteristic "angular distance" (in degrees) on earth below which two users are considered "close".

## Run serves
First, do :
```
cd ./vbbbq-backend
npm run dev
```
The backend server should start in development mode. This step is important, because in this mode the database adapter is in "Alter mode", in which the database structure can be altered ; and it will be automatically filled with VBBBQ's tables. In case of breaking update, this step can be reproduced but notice that data integrity is not guarantee ; you'll better dump your data and then manually make the required change .

Then, you can also start the frontend server in development mode, in a new terminal session :
```
cd ./vbbbq-frontend
npm run dev
```

Or stop the backend (`Ctrl+C`)  and start each of them in production mode with `npm run prod`.

## Going further
VBBBQ is based on two powerful web frameworks - with great possibilities - and one material package : SailsJS, Vue.js and Vuetify (+ additionnal packages like Sharp or vue-draggable-resizable). It's code is poorly commented (sorry for that) but well structured. Please refer to the documentations of those projects.

> VBBBQ is in alpha. That means issues can exist, and all imagined aspects are not implemented yet. Still, the core features are already fully usable. Investment of it's designer in future improvements will depends of community's reception and help.

